;(function(){

  // var images = $('.product-slider__img');
  var productBg = $('.product[data-prod="'+currentPage+'"] .product__bg');
  var title = $('.product[data-prod="'+currentPage+'"] .product-info__name');
  var images = $('.product[data-prod="'+currentPage+'"] .product-slider__img');
  var descr = $('.product[data-prod="'+currentPage+'"] .product-info__descr');
  var imagesNum = images.length;
  // var dots = $('.product-slider__controls');
  var dots = $('.product[data-prod="'+currentPage+'"] .product-slider__controls');
  var dot = $('.product-slider__dot');
  var current = 0;

  function currentImg(){
    dot.removeClass('product-slider__dot--active');
    $(dot[current]).addClass('product-slider__dot--active');
    // $(images).fadeOut();
    $(images).animate({
      "opacity": "0"
    }, 500)

    // $(images[current]).fadeIn(700);
    $(images[current]).animate({
      "opacity": "1"
    }, 500)
  }

  function currentProd(){

    // $(productBg).fadeOut();
    $(productBg).animate({
      "width": "0"
    }, 500)
    // $(title).fadeOut();
    $(title).animate({
      "opacity": "0",
      "right": "-30px"
    }, 500)

    $(descr).animate({
      "opacity": "0",
      "top": "30px"
    }, 500)


    // $(productBg[current]).slideDown(500);
    $(productBg[current]).animate({
      "width": "59%"
    }, 500)
    // $(title[current]).fadeIn();
    $(title[current]).animate({
      "opacity": "1",
      "right": "0"
    }, 500)

    $(descr[current]).animate({
      "opacity": "1",
      "top": "0"
    }, 500)
  }

  function setDots(){
    for( var i = 0; i < imagesNum; i++ ){
      dots.append('<div class="product-slider__dot" data-num="'+i+'">');
      dot = $('.product-slider__dot');
    }
  }


  function changeImg(){
    newCurrent = $(this).attr('data-num');
    if( current !== newCurrent ){
      current = newCurrent;
      currentImg();
    }
  }

  setDots();
  currentProd();
  currentImg();
  dot.eq(0).addClass('product-slider__dot--active');

  $('.product-slider__dot').click(changeImg)



  $(window).bind('hashchange', function() {
    images = $('.product[data-prod="'+currentPage+'"] .product-slider__img');
    dots = $('.product[data-prod="'+currentPage+'"] .product-slider__controls');
    productBg = $('.product[data-prod="'+currentPage+'"] .product__bg');
    title = $('.product[data-prod="'+currentPage+'"] .product-info__name');
    descr = $('.product[data-prod="'+currentPage+'"] .product-info__descr');
    dot.remove();
    setDots();
    currentProd();
    currentImg();
    $('.product-slider__dot').click(changeImg)
  });

}());
