;(function(){

  window.currentPage = window.location.hash;
  var product = $('.product');
  var catalog = $('.catalog__product');
  var arr = $('.catalog-control');
  var prevArr = $('#prevProd');
  var nextArr = $('#nextProd');
  // var arrows = ['#air7e', '#dacom', '#x1t'];
  var arrows = [
    {
      hash: '#air7e',
      name: 'Air 7e',
      img: 'img/catalog/1.png'
    },
    {
      hash: '#dacom',
      name: 'dacom twins',
      img: 'img/catalog/2.png'
    },
    {
      hash: '#tws',
      name: 'tws b1',
      img: 'img/catalog/3.png'
    },
    {
      hash: '#d900',
      name: 'd900 mini',
      img: 'img/catalog/4.png'
    },
    {
      hash: '#q29',
      name: 'q 29',
      img: 'img/catalog/5.png'
    },
    {
      hash: '#x1t',
      name: 'x1t',
      img: 'img/catalog/6.png'
    }
  ];

  function setCurrent(){
    for (var i = 0; i < product.length; i++) {
      if( $(product[i]).attr('data-prod') === window.currentPage ){
        product.removeClass('product--shown');
        $(product[i]).addClass('product--shown');
      }
    }
  }

  function setArrows(){
    for (var i = 0; i < arrows.length; i++) {
      if(window.currentPage === arrows[i].hash){
        var prev;
        i === 0 ? prev = arrows.length-1 : prev = i-1;
        var next;
        i === arrows.length-1 ? next = 0 : next = i+1;
        prevArr.attr('href', arrows[prev].hash);
        prevArr.find('.catalog-control__img').attr('src', arrows[prev].img);
        prevArr.find('.catalog-control__name').text(arrows[prev].name);
        nextArr.attr('href', arrows[next].hash)
        nextArr.find('.catalog-control__img').attr('src', arrows[next].img);
        nextArr.find('.catalog-control__name').text(arrows[next].name);
      }
    }
  }

  if(
    window.currentPage !== arrows[0].hash && window.currentPage !== arrows[1].hash && window.currentPage !== arrows[2].hash && window.currentPage !== arrows[3].hash && window.currentPage !== arrows[4].hash && window.currentPage !== arrows[5].hash
  ){
    window.currentPage = '#air7e';
  }

  setCurrent();
  setArrows();

  $(window).bind('hashchange', function() {
    window.currentPage = window.location.hash;
    setCurrent();
    setArrows();
  });


}());
