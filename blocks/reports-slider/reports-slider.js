;(function(){

  var slides = $('.reports-slider__slides');
  var slide = $('.reports-slider__slide');
  // var width = $('.reports-slider').width();
  var prev = $("#prevReportSlide");
  var next = $("#nextReportSlide");
  var currentSpan = $('#currentSlide');
  var allSpan = $('#allSlides');
  var current = 0;
  var all = slide.length;

  function setCurrent(  ){
    currentSpan.html(current+1+' ');
    slide.css({
      "height": 0,
      "opacity": 0
    });
    $(slide[current]).css({
      "height": "auto",
      "opacity": 1
    })
  }

  function nextSlide(){
    if( current < all-1 ){
      current += 1;
      console.log( current );
      setCurrent();
    }else if( current === all-1 ){
      current = 0;
      setCurrent();
    }
  }

  function prevSlide(){
    if( current > 0 ){
      current -= 1;
      console.log( current );
      setCurrent();
    }else if( current === 0 ){
      current = all-1;
      setCurrent();
    }
  }


  allSpan.html(' '+all);
  setCurrent();

  next.click( function(){
    nextSlide();
  } )
  prev.click( function(){
    prevSlide();
  } )

  // slide.width( width );
  // slides.width( slide.length * width );
  // $('.reports-slider__slide:last-child').prependTo(slides);
  // slides.css('left', -width);

  // next.click(function(){
  //   slides.animate({
  //     'margin-left': -width
  //   }, 500, function(){
  //     slides.css('margin-left', 0);
  //     $('.reports-slider__slide:first-child').appendTo(slides);
  //   })
  // });
  //
  // prev.click(function(){
  //   slides.animate({
  //     'margin-left': width
  //   }, 500, function(){
  //     slides.css('margin-left', 0);
  //     $('.reports-slider__slide:last-child').prependTo(slides);
  //   })
  // })

}());
