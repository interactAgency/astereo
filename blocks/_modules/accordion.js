;(function($){

  $.fn.interAccordion = function(options){

    var options = $.extend({
      speed: 400,
      togglerMod: '--mod',
      contentMod: '--mod'
    }, options)

    var selector = this;

    var make = function(){

      $(this).click(function(){

        if( $(this).hasClass( options.togglerMod ) ){
          $(this).removeClass( options.togglerMod );
          $(this).next().slideUp();
        }else{
          $( selector ).each(function(){
            $(this).removeClass( options.togglerMod );
            $(this).next().slideUp();
          })
          $(this).addClass( options.togglerMod )
          $(this).next().slideDown();
        }

      })

    }

    return this.each( make );

  }

}(jQuery));
