;(function(){

  var openBtn = $('.btn[data-type="order"]');
  var closeBtn = $('#closeOrder');
  var modal = $('.order');

  var deliveryRes = $(".delivery-choose");
  var deliveryChoose = $("#deliveryChoose");
  var np = $("#novaposhtaChoose");
  var courier = $("#courierChoose");
  var pickup = $("#pickupChoose");
  var form = $('#orderForm');

  function openModal(){
    modal.addClass('order--opened');
  }

  function closeModal(){
    modal.removeClass('order--opened');
  }

  openBtn.click( openModal );
  closeBtn.click( closeModal );

  deliveryChoose.change( function(){
    var chosen = $(this).val();
    if( chosen !== '0' ){
      deliveryRes.removeClass('delivery-choose--chosen');
      $(chosen).addClass('delivery-choose--chosen')
    }
  } )

  form.submit( function(e){
    e.preventDefault();
    console.log( $(this).serialize() );
  } )




}());
