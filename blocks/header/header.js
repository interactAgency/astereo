;(function(){

  var header = $('.header');
  var headerMod = 'header--scrolled';
  var logoImg = $('.header__logo-img');

  var goodsBtn = $('#goodsBtn');
  var aside = $('.aside');

  goodsBtn.click(function(){
    aside.toggleClass('aside--opened');
    $('.page').toggleClass('page--noScroll');
    header.addClass(headerMod);
    logoImg.attr('src', "img/logo/logo-black.png");
    goodsBtn.addClass('btn--accent');
  });

  $(window).scroll( function(){
    if( $(this).scrollTop() > 100 ){
      header.addClass(headerMod);
      logoImg.attr('src', "img/logo/logo-black.png");
      goodsBtn.addClass('btn--accent');
    }else{
      header.removeClass(headerMod);
      logoImg.attr('src', "img/logo/logo.png");
      goodsBtn.removeClass('btn--accent');
    }
  } );

}());
