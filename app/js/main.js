;(function($){

  $.fn.interAccordion = function(options){

    var options = $.extend({
      speed: 400,
      togglerMod: '--mod',
      contentMod: '--mod'
    }, options)

    var selector = this;

    var make = function(){

      $(this).click(function(){

        if( $(this).hasClass( options.togglerMod ) ){
          $(this).removeClass( options.togglerMod );
          $(this).next().slideUp();
        }else{
          $( selector ).each(function(){
            $(this).removeClass( options.togglerMod );
            $(this).next().slideUp();
          })
          $(this).addClass( options.togglerMod )
          $(this).next().slideDown();
        }

      })

    }

    return this.each( make );

  }

}(jQuery));

;(function(){

  window.currentPage = window.location.hash;
  var product = $('.product');
  var catalog = $('.catalog__product');
  var arr = $('.catalog-control');
  var prevArr = $('#prevProd');
  var nextArr = $('#nextProd');
  // var arrows = ['#air7e', '#dacom', '#x1t'];
  var arrows = [
    {
      hash: '#air7e',
      name: 'Air 7e',
      img: 'img/catalog/1.png'
    },
    {
      hash: '#dacom',
      name: 'dacom twins',
      img: 'img/catalog/2.png'
    },
    {
      hash: '#tws',
      name: 'tws b1',
      img: 'img/catalog/3.png'
    },
    {
      hash: '#d900',
      name: 'd900 mini',
      img: 'img/catalog/4.png'
    },
    {
      hash: '#q29',
      name: 'q 29',
      img: 'img/catalog/5.png'
    },
    {
      hash: '#x1t',
      name: 'x1t',
      img: 'img/catalog/6.png'
    }
  ];

  function setCurrent(){
    for (var i = 0; i < product.length; i++) {
      if( $(product[i]).attr('data-prod') === window.currentPage ){
        product.removeClass('product--shown');
        $(product[i]).addClass('product--shown');
      }
    }
  }

  function setArrows(){
    for (var i = 0; i < arrows.length; i++) {
      if(window.currentPage === arrows[i].hash){
        var prev;
        i === 0 ? prev = arrows.length-1 : prev = i-1;
        var next;
        i === arrows.length-1 ? next = 0 : next = i+1;
        prevArr.attr('href', arrows[prev].hash);
        prevArr.find('.catalog-control__img').attr('src', arrows[prev].img);
        prevArr.find('.catalog-control__name').text(arrows[prev].name);
        nextArr.attr('href', arrows[next].hash)
        nextArr.find('.catalog-control__img').attr('src', arrows[next].img);
        nextArr.find('.catalog-control__name').text(arrows[next].name);
      }
    }
  }

  if(
    window.currentPage !== arrows[0].hash && window.currentPage !== arrows[1].hash && window.currentPage !== arrows[2].hash && window.currentPage !== arrows[3].hash && window.currentPage !== arrows[4].hash && window.currentPage !== arrows[5].hash
  ){
    window.currentPage = '#air7e';
  }

  setCurrent();
  setArrows();

  $(window).bind('hashchange', function() {
    window.currentPage = window.location.hash;
    setCurrent();
    setArrows();
  });


}());

;(function(){

  var header = $('.header');
  var headerMod = 'header--scrolled';
  var logoImg = $('.header__logo-img');

  var goodsBtn = $('#goodsBtn');
  var aside = $('.aside');

  goodsBtn.click(function(){
    aside.toggleClass('aside--opened');
    $('.page').toggleClass('page--noScroll');
    header.addClass(headerMod);
    logoImg.attr('src', "img/logo/logo-black.png");
    goodsBtn.addClass('btn--accent');
  });

  $(window).scroll( function(){
    if( $(this).scrollTop() > 100 ){
      header.addClass(headerMod);
      logoImg.attr('src', "img/logo/logo-black.png");
      goodsBtn.addClass('btn--accent');
    }else{
      header.removeClass(headerMod);
      logoImg.attr('src', "img/logo/logo.png");
      goodsBtn.removeClass('btn--accent');
    }
  } );

}());

;(function(){

  var openBtn = $('.btn[data-type="order"]');
  var closeBtn = $('#closeOrder');
  var modal = $('.order');

  var deliveryRes = $(".delivery-choose");
  var deliveryChoose = $("#deliveryChoose");
  var np = $("#novaposhtaChoose");
  var courier = $("#courierChoose");
  var pickup = $("#pickupChoose");
  var form = $('#orderForm');

  function openModal(){
    modal.addClass('order--opened');
  }

  function closeModal(){
    modal.removeClass('order--opened');
  }

  openBtn.click( openModal );
  closeBtn.click( closeModal );

  deliveryChoose.change( function(){
    var chosen = $(this).val();
    if( chosen !== '0' ){
      deliveryRes.removeClass('delivery-choose--chosen');
      $(chosen).addClass('delivery-choose--chosen')
    }
  } )

  form.submit( function(e){
    e.preventDefault();
    console.log( $(this).serialize() );
  } )




}());

;(function(){

  // var images = $('.product-slider__img');
  var productBg = $('.product[data-prod="'+currentPage+'"] .product__bg');
  var title = $('.product[data-prod="'+currentPage+'"] .product-info__name');
  var images = $('.product[data-prod="'+currentPage+'"] .product-slider__img');
  var descr = $('.product[data-prod="'+currentPage+'"] .product-info__descr');
  var imagesNum = images.length;
  // var dots = $('.product-slider__controls');
  var dots = $('.product[data-prod="'+currentPage+'"] .product-slider__controls');
  var dot = $('.product-slider__dot');
  var current = 0;

  function currentImg(){
    dot.removeClass('product-slider__dot--active');
    $(dot[current]).addClass('product-slider__dot--active');
    // $(images).fadeOut();
    $(images).animate({
      "opacity": "0"
    }, 500)

    // $(images[current]).fadeIn(700);
    $(images[current]).animate({
      "opacity": "1"
    }, 500)
  }

  function currentProd(){

    // $(productBg).fadeOut();
    $(productBg).animate({
      "width": "0"
    }, 500)
    // $(title).fadeOut();
    $(title).animate({
      "opacity": "0",
      "right": "-30px"
    }, 500)

    $(descr).animate({
      "opacity": "0",
      "top": "30px"
    }, 500)


    // $(productBg[current]).slideDown(500);
    $(productBg[current]).animate({
      "width": "59%"
    }, 500)
    // $(title[current]).fadeIn();
    $(title[current]).animate({
      "opacity": "1",
      "right": "0"
    }, 500)

    $(descr[current]).animate({
      "opacity": "1",
      "top": "0"
    }, 500)
  }

  function setDots(){
    for( var i = 0; i < imagesNum; i++ ){
      dots.append('<div class="product-slider__dot" data-num="'+i+'">');
      dot = $('.product-slider__dot');
    }
  }


  function changeImg(){
    newCurrent = $(this).attr('data-num');
    if( current !== newCurrent ){
      current = newCurrent;
      currentImg();
    }
  }

  setDots();
  currentProd();
  currentImg();
  dot.eq(0).addClass('product-slider__dot--active');

  $('.product-slider__dot').click(changeImg)



  $(window).bind('hashchange', function() {
    images = $('.product[data-prod="'+currentPage+'"] .product-slider__img');
    dots = $('.product[data-prod="'+currentPage+'"] .product-slider__controls');
    productBg = $('.product[data-prod="'+currentPage+'"] .product__bg');
    title = $('.product[data-prod="'+currentPage+'"] .product-info__name');
    descr = $('.product[data-prod="'+currentPage+'"] .product-info__descr');
    dot.remove();
    setDots();
    currentProd();
    currentImg();
    $('.product-slider__dot').click(changeImg)
  });

}());

;(function(){

  var slides = $('.reports-slider__slides');
  var slide = $('.reports-slider__slide');
  // var width = $('.reports-slider').width();
  var prev = $("#prevReportSlide");
  var next = $("#nextReportSlide");
  var currentSpan = $('#currentSlide');
  var allSpan = $('#allSlides');
  var current = 0;
  var all = slide.length;

  function setCurrent(  ){
    currentSpan.html(current+1+' ');
    slide.css({
      "height": 0,
      "opacity": 0
    });
    $(slide[current]).css({
      "height": "auto",
      "opacity": 1
    })
  }

  function nextSlide(){
    if( current < all-1 ){
      current += 1;
      console.log( current );
      setCurrent();
    }else if( current === all-1 ){
      current = 0;
      setCurrent();
    }
  }

  function prevSlide(){
    if( current > 0 ){
      current -= 1;
      console.log( current );
      setCurrent();
    }else if( current === 0 ){
      current = all-1;
      setCurrent();
    }
  }


  allSpan.html(' '+all);
  setCurrent();

  next.click( function(){
    nextSlide();
  } )
  prev.click( function(){
    prevSlide();
  } )

  // slide.width( width );
  // slides.width( slide.length * width );
  // $('.reports-slider__slide:last-child').prependTo(slides);
  // slides.css('left', -width);

  // next.click(function(){
  //   slides.animate({
  //     'margin-left': -width
  //   }, 500, function(){
  //     slides.css('margin-left', 0);
  //     $('.reports-slider__slide:first-child').appendTo(slides);
  //   })
  // });
  //
  // prev.click(function(){
  //   slides.animate({
  //     'margin-left': width
  //   }, 500, function(){
  //     slides.css('margin-left', 0);
  //     $('.reports-slider__slide:last-child').prependTo(slides);
  //   })
  // })

}());
